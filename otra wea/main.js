Vue.component('task-list', {
    template: `
    <div>
    <task v-for="task in tasks">{{ task.task }}</task>
    </div>
`,

    data() {
        return {
            tasks: [
                { task: 'Go to the store', comoleted: true },
                { task: 'Kill to Kisame', comoleted: false },
                { task: 'Make to the foot', comoleted: true },
                { task: 'Sleep all day', comoleted: false },
            ]
        };
    }
});

Vue.component('task', {
    template: '<li><slot></slot></li>',


});

new Vue({
    el: '#root'
});